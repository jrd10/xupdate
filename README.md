# xupdate

Linux system update batch with options. See `xupdate.sh -h`
Apt commands are retourned in `xupdate.log`.

Inital file from `backus`: https://tricassinux.org/forum/d/219-exemple-de-script-bash-avec-options


# License 
See the License file. Only applicable for this specific adaptation.


# Update: 2021-01-27
