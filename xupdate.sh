#!/bin/bash

# Source : Backus :https://tricassinux.org/forum/d/219-exemple-de-script-bash-avec-options   
# Linux system update batch with options. 
# See 'xupdate.sh -h' for details | Make it executable:`chmod +x xupdate.sh`
# jrd10 | 20210/27 | Project et licence at https://gitlab.com/jrd10/xupdate 
#

# Current repertory and bash file name from $0 variable
pwd_cmd=$(pwd)

echo 
echo "----------- Scritp beginning" $pwd_cmd"/"$0 "--------------"
echo

# create a xupdate.log file starting with date command and updated at each run
# Copy previous log in a new file if you need to save it
echo "--------------------------------"        >  $pwd_cmd/xupdate.log   # Create the file or erase an existing one 
date                                           >> $pwd_cmd/xupdate.log
echo "Log in xupdate.log (Erased at each run)" >> $pwd_cmd/xupdate.log
echo "File:" $pwd_cmd"/xupdate.log "           >> $pwd_cmd/xupdate.log   # Wrong repertory ???
echo "--------------------------------"        >> $pwd_cmd/xupdate.log

# See to have the long command in a variable

while [ -n "$1" ]; do # while loop starts

     case "$1" in

     -h) echo "-h   Help, show all the functions and arguments"
         echo "-r   Restart your PC after update"
         echo "-p   Poweroff your PC after update"
         echo "Please, note the log will be saved in `xupdate.log` file"
         exit 1 ;;

     -r) sudo apt -y update && sudo apt -y full-upgrade && sudo apt -y autoremove --purge && sudo apt -y clean && sudo apt -y autoclean && sudo reboot ;; # Redémarrage en fin de script
         # To be done: redirection to `xupdate.log`

     -p) sudo apt -y update && sudo apt -y full-upgrade && sudo apt -y autoremove --purge && sudo apt -y clean && sudo apt -y autoclean && sudo poweroff ;; # Extinction en fin de script
         # To be done: redirection to `xupdate.log`

     *)

     esac

     shift

done

# Run all the commands without reboot neither poweroff

sudo apt-get -y update  >> xupdate.log && sudo apt-get -y full-upgrade >> xupdate.log && sudo apt-get -y autoremove --purge >> xupdate.log && sudo apt -y clean && sudo apt -y autoclean

# Logs from apt commands
cat $pwd_cmd/xupdate.log

echo
echo "------------------- End of script" $0 "---------------------" 
echo
